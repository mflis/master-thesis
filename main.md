---
titlePL: Linkowanie do bazy wiedzy w języku polskim
titleEN: Entity linking in Polish
author: Marcin Flis
supervisor: dr inż. Aleksander Smywiński-Pohl
acknowledgements: Serdecznie dziękuję dr Smywińskiemu-Pohlowi za poświęcony czas i merytoryczną pomoc w realizacji pracy
year: 2019
faculty: Wydział Informatyki, Elektroniki i Telekomunikacji
department: Katedra Informatyki
documentclass: report
codeBlockCaptions: true
eqnPrefix: 
secPrefix: 
tblPrefix: 
figPrefix: 
bibliography: bibliography.bib
header-includes: |
  ```{=latex}
  \usepackage{longtable}
  \newcommand{\x}{{\mathbf x}}
  \newcommand{\z}{{\mathbf z}}
  \newcommand{\y}{{\mathbf y}}
  \newcommand{\mat}[1]{{\mathbf #1}}
  \DeclareMathOperator*{\argmin}{arg\,min}
  \DeclareMathOperator*{\argmax}{arg\,max}
  \setlist[description]{leftmargin=1cm,labelindent=1cm}
  ```
---




# Wprowadzenie

_Entity Linking_ jest zadaniem polegającym na powiązaniu fragmentów tekstu
z  właściwymi odnośnikami w bazie wiedzy. Te jednostki tekstu określane są
terminem _Named Entities_ i odnoszą się do znanych osób, organizacji, miejsc,
etc. Najczęściej używaną bazą wiedzy
jest Wikipedia (w takim przypadku stosuje się też termin _wikification_). Jest ona
chętnie wybierana, ponieważ zawiera ogromną bazę artykułów: obecnie ok. 1 350 000
artykułów dla polskiej Wikipedii -@polish-wiki-size i ok. 5 900 000 artykułów
dla angielskiej wersji -@english-wiki-size. Dodatkowo baza artykułów jest ciągle
rozszerzana i aktualizowana przez sporą rzeszę aktywnych użytkowników.
Alternatywne terminy, które odnoszą się do tego zadania to:  *Named Entity
Linking*(NEL), _Named Entity Disambiguation_(NED), _Named Entity
Normalization_(NEN)

> **TODO:** Wikipedia podaje tu przykłady paperów, gdzie występują te
określenia. Czy je cytować u siebie?

Dla przykładu w zdaniu _Przemówienie prezydenta odbyło się na zamku_ zadaniem
algorytmu jest stwierdzenie, czy _prezydenta_ odnosi się do prezydenta Warszawy, czy
prezydenta Polski.

Zadanie jest zdefiniowane w dwóch wariantach. W pierwszym dane wejściowe to
tekst bez żadnych oznaczeń (ang. annotations). Zadaniem algorytmu jest
zarówno identyfikacja named entities, jak również przypisanie im odnośników
w bazie wiedzy. W drugim wariancie zadaniem jest jedynie powiązanie named
entities z bazą wiedzy, same named entities są oznaczone w tekście wejściowym.
Wariant drugi występuje częściej w publikacjach naukowych oraz konkursach, jest
on również zastosowany w tej pracy.

## Zastosowania

Entity Linking jest używane przede wszystkim jako komponent systemów, które
służą do zrozumienia i analizy tekstu, np. czatboty, systemy do rekomendacji lub
wyszukiwania semantycznego. Możliwość wyodrębnienia konceptów na poziomie
semantycznych daje głębsze zrozumienie tekstu.

Dla przykładu  wyniki wyszukiwania dla frazy _prezydent Polski_ zwrócą strony
nie tylko zawierające powyższe słowa, ale również te z frazą _Andrzej Duda_
i odfiltrują artykuły na temat Prezydenta Stanów Zjednoczonych.
Zrozumienie na poziomie koncepcyjnym intencji wyszukującego pozwala też
zaprezentować schematyczny skrót informacji na poszukiwany temat, np
infobox z Wikipedii 

![Przykład zrozumienia zapytania na poziomie semantycznym przez wyszukiwarkę
Google. W fragmencie tekstu wyróżnione są nie tylko słowa _prezydent Polski_
użyte w zapytaniu, ale również imię i nazwisko obecnie urzędującego prezydenta.
](images/google-duda){height=40% width=40%}


## Cele pracy
Celem pracy jest stworzenie algorytmu Entity Linking opierającego się  na
wektorowych reprezentacjach słów  oraz porównanie jego skuteczności
z tradycyjnym systemem opierającym się na statystykach wystąpień wewnętrznych
linków w korpusie Wikipedii.



# Aktualny stan badań

## Słownik
- __odnośnik w bazie wiedzy__(ang. entity) - jednoznaczny identyfikator konceptu
  w bazie wiedzy. W tej pracy bazą wiedzy jest zbiór artykułów w polskiej wersji
  Wikipedii, a odnośnik to wewnętrzny identyfikator artykułu, np. dla artykułu
  _AWK_ ma on wartość $2$.  Identyfikator można skonwertować na link w postaci
  _https://pl.wikipedia.org/?curid={wikipedia_id}_ 
  W ramach tej pracy terminy _odnośnik_ i _artykuł_ są równoważne.
- __wzmianka__ (ang. mention) - fragment tekstu, dla którego należy znaleźć
  właściwy odnośnik w bazie wiedzy
- __kandydat__ -  odnośnik w bazie wiedzy, który został wybrany  przez pierwszą
  fazę algorytmu jako potencjalne dopasowanie dla wzmianki
- __indeks wzmianka-odnośnik__ - statystyka liczności powiązań pomiędzy wzmiankami
  i odnośnikami. W przypadku Wikipedii powstaje na podstawie struktury
  wewnętrznych linków pomiędzy artykułami. Z całego korpusu Wikipedii
  ekstrahowane są linki wewnętrzne, następnie dokonuje się zliczenia jak często
  dany fragment tekstu linkuje do różnych artykułów.
- __osadzenia słów__ (ang. word embeddings) - sposób reprezentacji słów za pomocą
  nisko-wymiarowych(zazwyczaj 50-300 wymiarów) wektorów liczb rzeczywistych. Koncept
  został po raz pierwszy w pracy (@mikolov2013efficient). Od tego czasu powstało
  wiele prac rozwijających ten koncept.
- __dokument__ - spójny tekst pochodzący z jednego źródła, na którym dokonywana
  jest operacja Entity Linking. Przykłady dokumentów to artykuł na Wikipedii,
  artykuł prasowy, fragment książki. Modele, które opierają się na spójności
  odnośników w dokumencie zakładają, że dokument posiada _motyw przewodni_ i starają
  się dobierać takie odnośniki do wzmianek, które są zgodne z motywem
  przewodnim.
- __kontekst lokalny__ - $N$ słów występujących w dokumencie bezpośrednio przed
  i po wzmiance. Mogą być usunięte nieistotne słowa (ang. stopwords).

![Ilustracja zdania z oznaczonymi wzmiankami oraz listami kandydatów](images/entity_linking)


## Schemat algorytmu {#sec:ogolny_schemat}

Algorytm Entity Linking można podzielić na dwie zasadnicze fazy:

 - wyznaczenie zbioru potencjalnych kandydatów, odpowiednich dla danej wzmianki.
   Krok ten jest konieczny, aby ograniczyć liczbę rozważanych odnośników z kilku
   milionów występujących w całej bazie wiedzy do kilku-kilkunastu.
   W najprostszym wariancie wykorzystuje się w tym celu indeks wzmianka-odnośnik
   wybierając kilka artykułów, które najczęściej są powiązane z daną wzmianką.
   Takie dokładne dopasowanie ma zauważalne braki w przypadku języka polskiego.
   Z racji bogactwa form wyrazowych  w  języku polskim, w znaczącej
   liczbie przypadków opisana metoda nie znajduje żadnych potencjalnych
   kandydatów. Ulepszona wersja tej metody będzie opisana w dalszej części
   pracy.
 - Dopasowanie właściwego odnośnika spośród zbioru kandydatów wyznaczonych
   w poprzednim kroku. W literaturze można odnaleźć wiele sposobów na wykonanie
   tego kroku, zarówno opierających się na cechach statystycznych jak i modele
   wykorzystujące sieci neuronowe i osadzenia słów.

## Modele statystyczne

Modele statystyczne opierają się na różnorodnych statystykach obliczanych  na
podstawie indeksu wzmianka-odnośnik. Najbardziej podstawowy model wybiera
kandydata, który jest najczęściej linkowany do danej wzmianki.
(@milne2008learning) wykorzystuje dwuetapowy algorytm, w którym najpierw
identyfikuje pojęcia jednoznaczne w dokumencie, a następnie  na ich podstawie
oblicza semantyczne podobieństwo (ang. relatedness) każdego z kandydatów 
do uśrednionego sensu wyrażeń jednoznacznych. Ta cecha wzbogaca naiwny algorytm.

$$ WLM(e_1, e_2) = 
        1 - \frac{\log\,\max(|C_{e_1}|, |C_{e_2}|) - \log|C_{e_1} \cap C_{e_2}|}
                 {\log|E| - \log\,\min(|C_{e_1}|, |C_{e_2}|)} $$
gdzie:

$E$ 
  : zbiór wszystkich odnośników w bazie wiedzy

$e_1$, $e_2$
 :  wzmianki, pomiędzy którymi liczona jest miara podobieństwa semantycznego

$C_e$
 :  zbiór odnośników, które w swoich artykułach
    zawierają linki do odnośnika $e$


@Pohl dodaje m.in prawdopodobieństwo, że dana wzmianka jest linkiem
w Wikipedii. Dodane są również inne usprawnienia, np. dodanie do zbioru pojęć,
na podstawie których obliczany jest uśredniany sens również wzmianek niemal
jednoznacznych.

> **_TODO:_**  można bardziej rozpisać tą sekcję 

Przedstawione podejście wymaga stworzenia skomplikowanych statystyk na podstawie
korpusu Wikipedii. Poza tym problem stanowią krótsze dokumenty, gdzie nie ma
wielu odnośników. Trudniej jest wtedy dobrze wyznaczyć _motyw przewodni_
dokumentu na podstawie wyrażeń jednoznacznych.


## Modele neuronalne

Opisane wcześniej modele statystyczne skupiały się głównie na globalnej
spójności przypisanych odnośników w danym dokumencie. Zupełnie pomijały
one element spójności kandydata z kontekstem lokalnym. Używanie kontekstu
lokalnego wymaga kompatybilnej reprezentacji dla słów w kontekście wzmianki oraz 
kandydatów. W przypadku słów popularną w ostatnich latach reprezentacją są
osadzenia słów. @yamada najpierw tworzy osadzenia słów wykorzystując
model Skip-Gram, następnie w podobny sposób tworzy reprezentacje odnośników.
Rolę kontekstu pełnią tu artykuły, które linkują do danego artykułu. 
Zainspirowano się tutaj miarą relatedness i założeniem, że podobne koncepcyjnie
artykuły mają podobny zestaw stron, które do nich linkują. Algorytm uczy się
umieszczać takie artykuły blisko w przestrzeni wektorowej. Funkcja kosztu
stosowana przy tworzeniu osadzeń artykułów jest zdefiniowana we wzorze
(@eq:yamada_wiki_embed)

$$ \mathcal{L}_e = \sum_{e_i \in E}\sum_{e_o \in C_{e_i}, e_i \neq e_o}\log
P(e_o|e_i)  $$ {#eq:yamada_wiki_embed}
$$ P(e_o|e_i) = \frac{\exp(\mathbf{V}_{e_i}\!^\top \mathbf{U}_{e_o})}{\sum_{e
\in E}\exp(\mathbf{V}_{e_i}\!^\top \mathbf{U}_e)} $$

gdzie:

$E$
 :  zbiór wszystkich artykułów w Wikipedii danego języka

 $e_i$, $e_o$
 :  artykuły w Wikipedii

$C_{e_i}$
 :  zbiór artykułów, które zawierają linki do artykułu $e_i$

$\mathbf{V}_e$,$\mathbf{U}_e$
 :  reprezentacje wektorowe artykułu $e$, które są uczone

Ostatni krok ma na celu sprawienie, aby podobne koncepcyjnie słowa i odnośniki
znajdowały się blisko siebie w przestrzeni wektorowej.
Bez tego te dwie grupy osadzeń mogą znaleźć się w innych miejscach przestrzeni
wektorowej i metody ich porównań takie jak iloczyn skalarny (ang.dot-product) czy
miara kosinusowa (ang.cosine similarity) nie będą zwracały oczekiwanych rezultatów.
W tym celu wykorzystuje się kolejny model Skip-Gram, gdzie _słowem_ centralnym
jest artykuł, a jego kontekstem są słowa, które występują w pobliżu linków do
niego prowadzących. Wszystkie trzy modele są optymalizowane jednocześnie. W efekcie powstają
osadzenia słów i odnośników  w tej samej przestrzeni wektorowej.

Otrzymane osadzenia są wykorzystywane do modelowania podobieństwa pomiędzy
lokalnym kontekstem a kandydatami dla danej wzmianki. Lokalny kontekst tworzą
rzeczowniki w otoczeniu wzmianki. Są one  są uśrednianie i porównywane
z kontekstem wzmianki za pomocą miary kosinusowej. 

Osadzenia są wykorzystywane również w modelowaniu globalnej spójności odnośników
w dokumencie. Wszystkie wektory stanowiące kontekst globalny są uśredniane
i porównywane z kandydatami za pomocą miary kosinusowej. W pierwszym kroku
globalny kontekst stanowią wszystkie jednoznaczne odnośniki. Przy użyciu tego
kontekstu i pozostałych cech dopasowywane są odnośniki do wzmianek. W drugim
kroku wszystkie odnośniki wyznaczone w pierwszym kroku stanowią kontekst
globalny i odnośniki są wyznaczane ponownie.

Poza kontekstem lokalnym i globalnym są używane cechy oparte
o statystyki wystąpień w Wikipedii i porównywanie tekstowe tytułu kandydata
i wzmianki.

\vskip 1cm 

Inną pracą, która modeluje zarówno kontekst lokalny jak i globalny jest
(@ganea2017deep). Podobnie jak @yamada modelowanie lokalnego
kontekstu opiera się na osadzeniu słów oraz odnośników we wspólnej przestrzeni
wektorowej, ale proces ich trenowania jest inny. 

Zamiast trenować własne osadzenia słów, wykorzystane zostały
gotowe osadzenia word2vec wytrenowane na dużym korpusie tekstu.

Kolejną różnicą jest fakt, że do trenowania osadzeń artykułów nie użyto
standardowego modelu Skip-Gram. Zdefiniowane są dwie dystrybucje słów.
_Dystrybucja pozytywna_ jest dystrybucją warunkową słów występujących w pobliżu
odnośnika. Jest ona skonstruowana na podstawie zliczeń słów występujących
w pobliżu linków wewnętrznych odnoszących się do danego artykułu oraz słów
występujących w tym artykule. _Dystrybucja negatywna_ opisuje słowa niezwiązane
w danym odnośnikiem. Jest obliczana na podstawie słów występujących w całym
korpusie Wikipedii i jest identyczna dla wszystkich artykułów. Celem uczenia
jest sprawienie, że osadzenie odnośnika jest bardziej zbliżone w przestrzeni
wektorowej do osadzeń słów z dystrybucji pozytywnej niż do słów z dystrybucji
negatywnej. Funkcja kosztu zastosowana przy trenowaniu jest opisana wzorem
\ref{eq:ganea_cost_finc}

$$ J(\z; e) = \mathbb{E}_{w^+|e} \; \mathbb{E}_{w^-} \left[ h\left( \z;w^+,w^-
\right) \right]$$   {#eq:ganea_cost_finc} 
$$ h(\z;w,v) = \left[\gamma - \langle \z, \x_{w}-  \x_{v} \rangle \right]_+ $$

gdzie:

$\mathbb{E}_{w^+|e}$ , $\mathbb{E}_{w^-}$
: dystrybucje pozytywna i negatywna, z których są próbkowane słowa do dalszych obliczeń

$\gamma$
: konfigurowalny margines, gdzie $\gamma > 0$

$\z$
: trenowane osadzenie artykułu, gdzie $\|\z\|=1$

@yamada wybierając słowa uwzględniane w lokalnym kontekście
ograniczał je tylko do rzeczowników. W tej pracy zastosowano bardziej
wyrafinowane rozwianie oparte na mechanizmie uwagi (ang. attention) za pomocą
wzoru (@eq:ganea_attention)


$$ u(w) = \max_{e \in \Gamma(m)} \x_{e}^{\top} \mat A \x_{w} $$ {#eq:ganea_attention}

gdzie:

$A$
: diagonalna macierz, której wagi są uczone

$\Gamma(m)$
: zbiór kandydatów dla wzmianki $m$

$w$
: słowo z kontekstu lokalnego wzmianki $m$

$\x_e$, $\x_w$
: reprezentacje wektorowe odpowiednio kandydata i słowa

Na podstawie tak stworzonej macierzy podobieństw wybierane jest $topK$ słów
w kontekście, które korelują z przynajmniej jednym z kandydatów. Tak wybrane
słowa są łączone w jeden wektor za pomocą średniej ważonej składowych gdzie wagi
to wskaźnik podobieństwa $u(w)$ znormalizowany funkcją \mbox{softmax}. Wskaźnik
powiązania wzmianka-kandydat jest obliczany wg wzoru (@eq:ganea_final).
Następnie ten wskaźnik jest łączony z licznością wystąpień w indeksie
wzmianka-odnośnik za pomocą sieci neuronowej. Do modelowania kontekstu
globalnego został użyty model CRF.

$$\Psi(e,c) =  \sum_{w \in topK}  \beta(w) \; \x_{e}^\top \mat B \, \x_{w}$$
{#eq:ganea_final}
$$\beta(w) = \frac{ \exp[u(w)] }{ \sum_{v \in topK} \exp[u(v)] } $$

![ Ilustracja modelowania kontekstu lokalnego
w (@ganea2017deep)](images/att.png){height=60% width=100%}

> **_TODO:_**  Można tu jeszcze opisać _Improving Entity Linking by Modelling
Latent Relations between Mentions_ jako kolejną pracę w podobnym tonie oraz
_Deeptype_ jako przykład całkowicie różnego podejścia

# Zbiory danych

W tej pracy przetestowano opracowaną metodę na dwóch zbiorach danych:

  - korpus tekstów z  polskiej Wikipedii
  - korpus artykułów prasowych 

``` 
10002 Źródła  źródło  0 subst:pl:nom:n  _ _
10002 znajdują  znajdować 1 fin:pl:ter:imperf _ _
10002 się się 1 qub _ _
10002 w w 1 prep:loc:nwok _ _
10002 południowych  południowy  1 adj:pl:loc:n:pos  _ _
10002 Wogezach  Wogezach  1 subst:pl:loc:n  Wogezy  Q187843
10002 . . 0 interp  _ _

10002 Następnie następnie 0 adv _ _
10002 Mozela  Mozela  1 subst:sg:nom:f  _ _
10002 płynie  płynąć  1 fin:sg:ter:imperf _ _
10002 przez przez 1 prep:acc:nwok _ _
10002 Lotaryngię  Lotaryngia  1 subst:sg:acc:f  Lotaryngia  Q1137
10002 i i 1 conj  _ _
10002 Niemcy  Niemiec 1 subst:pl:nom:m1 _ _
10002 . . 0 interp  _ _
```
: Listing Przykład obrazujący schemat danych w obu korpusach  {#lst:data_example}

Oba zbiory zostały przygotowane na potrzeby konkursu Poleval 2019, w którym
wystąpiło zadanie dotyczące Entity Linking i  mają taką samą strukturę zaprezentowaną w @lst:data_example . Dane
w kolejnych kolumnach to:

- wewnętrzny identyfikator artykułu
- token 
- token poddany lematyzacji. Lematyzacja została wykonana automatycznie, więc
  pewien odsetek przykładów nie jest oznaczony poprawnie.
- wskaźnik białego znaku - $1$ oznacza, że  poprzedni token to biały
  znak(najczęściej spacja), $0$ w przeciwnym przypadku
- tytuł artykułu w Wikipedii, który jest powiązany ze wzmianką, której częścią
  jest dany token. Jeśli przy danym tokenie nie ma żadnego powiązanego artykułu
  stosuje się znak podkreślenia.
- identyfikator  artykułu w Wikidata, który jest powiązany ze wzmianką, której częścią
  jest dany token. Jeśli przy danym tokenie nie ma żadnego powiązanego artykułu
  stosuje się znak podkreślenia. Istnieje mapowanie pomiędzy
  identyfikatorami artykułów w Wikipedii i Wikidata.




|                  | Wikipedia | Artykuły prasowe |
|------------------|-----------|------------------|
| ilość tokenów    | 460 mln   | 45 tys.          |
| ilość wzmianek   | 35 mln    | 3 tys.           |
| ilość dokumentów | 1,88 mln  | 2                |
| ilość zdań       | 19,1 mln  | 3 tys.           |

: Statystyki dostępnych korpusów 

Na podstawie analizy korpusu artykułów prasowych można stwierdzić pewien
artefakt związany z oznaczeniami przynależności do dokumentów. Prawdopodobnie
wszystkim tekstom z jednego czasopisma przypisano ten sam identyfikator
dokumentu, mimo że składają się na niego fragmenty artykułów o różnej tematyce. 
W związku z tym wykorzystanie cech opartych o globalną spójność wzmianek
w dokumencie prawdopodobnie nie przyniesie korzystnych rezultatów.

> **_TODO:_**  Opisać statystyki zbiorów (ilość tokenów, mentions), jakie dokładnie podzbiory zostały
użyte do wykonania eksperymentów, etc.

# Tworzenie listy kandydatów


Jak zostało to wcześniej opisane, pierwszym krokiem algorytmu entity linking
jest wybór kandydatów dla wzmianki. Zazwyczaj dokładny sposób ich pozyskiwania
nie jest opisywany w publikacjach. 

W pracy przeprowadzono porównanie sposobów generowania listy kandydatów opartych
na dokładnym dopasowaniu tekstowym wzmianki z indeksem stworzonym na bazie
korpusu artykułów Wikipedii oraz ulepszenia tej techniki oparte o niedokładne
dopasowanie oparte o analizatory językowe ElasticSearch.

Eksperymenty były przeprowadzane na podzbiorze korpusu artykułów prasowych. 


## Dokładne dopasowanie
 Jak wspomniano wcześniej w sekcji @sec:ogolny_schemat wyszukiwanie za pomocą
 dokładnego dopasowania tekstowego wzmianki nie daje zadowalających rezultatów
 dla języka polskiego. Z racji dużo bogatszej deklinacji niż w języku angielskim
 jest bardziej prawdopodobne, że dana forma wyrazu nie została nigdy użyta
 w korpusie jako link wewnętrzny. W takim przypadku algorytm zwróci pustą
 listę kandydatów i nie będzie możliwe dopasowanie właściwego odnośnika.
 W przypadku badanego podzbioru w 15 % przypadków lista kandydatów była pusta.

## Dopasowanie z wykorzystaniem Elasticsearch

Aby znaleźć rozwiązanie problemu pustej listy kandydatów, przeprowadzono
w ramach tej pracy serię eksperymentów z niedokładnym dopasowaniem wzmianki przy
użyciu Elasticsearch. 

Elasticsearch jest silnikiem do agregacji i analizowania danych tekstowych.
Posiada on zaawansowane opcje indeksowania i szybkiego wyszukiwania dokumentów
oparte na Apache Lucene. W kontekście tej pracy szczególnie przydatna była
funkcjonalność analizatorów. Pozwalają one na transformację tekstu dokumentów
przed ich zapisaniem oraz zapytań kierowanych do indeksu. Przykładowe
transformacje to zamiana wielkich liter na małe, usunięcie nieinformatywnych
słów (ang. stopwords) czy stemming. Stemming polega na usunięciu końcówki
fleksyjnej ze słowa. Po takiej operacji słowa _ulica_ i _ulicy_ są traktowane
jako równoważne.   W opisanych niżej eksperymentach został użyty analizator
Stempel dokonujący  stemmingu dla języka polskiego.  Pozwala to uzyskać niepustą
listę kandydatów dla wzmianek, które nigdy nie wystąpiły w korpusie Wikipedii
oraz poszerzyć listę kandydatów dla pozostałych wzmianek. Jednakże zastosowanie
tej metody rodzi dwa dodatkowe problemy:

> **_TODO:_**  Do pierwszego punktu można dodać schemat, bo ciężko to w słowach
jasno ująć.

- w jaki sposób szeregować listę kandydatów i zapewnić unikalność znajdujących
  się na niej odnośników. Przy dopasowaniu dokładnym lista jest szeregowana na
  podstawie ilości występujących w korpusie linków wewnętrznych, które miały
  dokładnie taki sam tekst jak wzmianka i linkowały do danego odnośnika. Przy
  dopasowaniu niedokładnym mamy wiele takich list, na których dany odnośnik może
  występować wiele razy, z innymi statystykami wystąpień. Aby uniknąć
  wielokrotnego występowania odnośnika na liście kandydatów, w tej pracy
  zdecydowano się zsumować statystyki ich wystąpień.
- większa ilość kandydatów oznacza więcej szumu przy wyborze właściwego
  odnośnika. Należy zrównoważyć konieczność znajdowania się w liście kandydatów
  właściwego odnośnika z unikaniem dużej listy niezwiązanych kandydatów. Opisane
  niżej eksperymenty służą znalezieniu rozwiązania tego problemu.

### Organizacja eksperymentu


- Do indeksu Elasticsearch wgrano dokumenty w postaci: 
`{"tekst wzmianki" : "id_do_listy_kandydatów"}` 
    
- Następnie z wybranego podzbioru korpusu ekstrahowano wzmianki i prawidłowy
  odnośnik
- Przy użyciu tekstu wzmianki konstruowane jest zapytanie do Elasticsearch,
  które zwraca listę dokumentów, których wzmianki zostały dopasowane do
  zapytania. 
- Lista jest filtrowana na podstawie aktualnie badanych kryteriów.
- Zwrócone dokumenty są kojarzone z odpowiadającymi im listami kandydatów.
- Listy kandydatów są łączone w jedną listę w opisany wcześniej sposób.
- Obliczane są statystyki skuteczności dopasowań.


### Warianty
> **_TODO:_**  Tu opisując wybieranie topN można pokazać dystrybucję długości pasujących 
list pasujących mentions, że są bardzo długie i trzeba je przyciąć. Poza tym te
z końca bywają dość odległe znaczeniowo.

Pierwszym testowanym wariantem było ograniczenie tylko do _topN_ pierwszych
wzmianek zwróconych z zapytania do Elasticsearch. Elasticsearch zwraca wyniki
uporządkowane pod względem istotności(ang. relevance score). Istotność może być obliczna na wiele
sposobów, ale w domyślnym przypadku jest to miara TF-IDF po dokonaniu
transformacji analizatorem(w tym przypadku jest to stemming).  Testowano warianty
z wartością $n$ od 1 do 5. 

Jednak w zwracanych listach wiele wzmianek ma tą samą wartość relevance score.
Tak więc pierwszy wariant może pominąć cześć wyników, które mają taki sam
relevance score jak wyniki, przeszły filtrowanie pozytywnie. Dlatego drugi
testowany wariant filtruje wyniki, których relevance score znajduje się w _topN_
 dla danego zapytania.

Kolejną obserwacją jest, że jeśli dokładne dopasowanie tekstowe istnieje, to
bardzo często zawiera ono również poprawny odnośnik na swojej liście kandydatów.
Dlatego zastosowano następującą heurystykę: jeśli na liście zwróconej
z Elasticsearch znajduje się wzmianka z dokładnym dopasowaniem tekstowym,
wszystkie pozostałe dopasowania są odrzucane. Ma ona na celu tworzenie krótszych
list kandydatów, a przez to  zmniejszenie szumu w dalszych krokach przetwarzania.

Przedstawiona poniżej tabela @tbl:elastic-experiments  i wykres
@fig:elastic-experiments pokazują statystyki przeprowadzonych eksperymentów.
Oznaczenia wariantów:

- **EX** - oznacza zastosowanie heurystyki polegającej na preferowaniu
  dokładnego dopasowania tekstowego
- **ES** - oznacza zastosowanie wartości relevance score przy filtrowaniu
  wyników
- **cyfra** -  oznacza ile pierwszych wyników było brane pod uwagę przy
  tworzeniu ujednoliconej listy

Na podstawie przedstawionych danych widać, że heurystyka z dokładnym
dopasowaniem zauważalnie zwiększa odsetek poprawnych kandydatów w top 1.
Najmniejszy odsetek list, na których nie występuje poprawny kandydat ma wariant
ES 5, ale dzieje się to kosztem długich list kandydatów. Poprawny odnośnik
w zauważalnej części przypadków znajduje się na 20 i dalszej pozycji.
Wariant z łączeniem list na podstawie wartości relevance z Elasticsearch daje
minimalnie lepsze wyniki od wariantu bazowego. Ostatecznie do dalszych badań
został wybrany wariant _EX ES 1_, z którego do dalszego przetwarzania jest
wybierane pierwsze 5 pozycji. Jest to  balans pomiędzy długością listy
kandydatów a odsetkiem przypadków, gdzie poprawny kandydat znajduje się na
liście.

| Wariant | top 1  | top 5  |
|---------|--------|--------|
| 1       | 74,4 % | 83,2 % |
| 5       | 75,6 % | 92,5 % |
| ES 1    | 75,8 % | 88,0 % |
| ES 5    | 63,1 % | 87,3 % |
| EX 1    | 81,2 % | 91,0 % |
| EX 5    | 79,6 % | 91,6 % |
| EX ES 1 | 81,3 % | 91,3 % |
| EX ES 5 | 78,6 % | 90,8 % |

: Statystyki wybranych eksperymentów z tworzeniem list kandydatów przy użyciu
Elasticsearch. **top1** oznacza odsetek przypadków, gdzie poprawny odnośnik
znajdował się na pierwszym miejscu listy. **top5** oznacza odsetek przypadków,
gdzie poprawny odnośnik znajdował się wśród pierwszych 5 miejsc listy.
{#tbl:elastic-experiments}

![Wykres przedstawiający, na którym miejscu w liście kandydatów uporządkowanej
na podstawie liczności w indeksie znajduje się poprawny odnośnik przy użyciu
zapytań do Elasticsearch z różnymi wariantami filtrowania. _absent_ oznacza, że
lista nie zawiera poprawnego odnośnika](images/elastic_experiments){#fig:elastic-experiments}

# Algorytm Entity Linking

Zastosowany algorytm jest wzorowany na @ganea2017deep. Tekst znajdujący się
w oknie wokół wzmianki jest mapowany na osadzenia. Tak samo mapowane są również
kandydujące odnośniki. Następnie najbardziej istotne słowa w lokalnym kontekście
są wyłaniane za pomocą mechanizmu attention opisanego w  @ganea2017deep. Następnie
wyłonione osadzenia są łączone w jeden wektor reprezentujący kontekst lokalny za
pomocą sumy ważonej. Przy użyciu tej reprezentacji kontekstu lokalnego jest
obliczany wskaźnik podobieństwa z  kandydatami za pomocą iloczynu
skalarnego (ang. dot product). Jako poprawny odnośnik jest wybierany kandydat z największą wartością wskaźnika
podobieństwa. Podobnie jak w @ganea2017deep macierze wag uczestniczące w obliczaniu attention
zaincjalizowano jako macierze diagonalne o wartości $1$.

> **TODO:** O tym nie było w artykule, wyczytałem to z kodu, trzeba to jakoś
zaznaczyć?


Względem @ganea2017deep różna była metoda pozyskiwania osadzeń słów. Jest
opisana niżej.

## Tworzenie osadzeń słów i artykułów dla polskiej Wikipedii
1. Algorytmem opisanym w @Pohl dokonano ujednoznacznienia całego korpusu
   Wikipedii, tzn fragmentom tekstu, które nie są linkami wewnętrznymi, ale
   odnoszą się konceptów, dla których występują artykuły w Wikipedii przypisano
   najbardziej prawdopodobny link. Zgodnie z wytycznymi stylistycznymi Wikipedii
   dany artykuł powinien być linkowany w tekście tylko raz @repeated-links.
   Zastosowanie ujednoznacznienia zapewnia znaczący wzrost ilości wzmianek dla
   artykułów w Wikipedii. Umożliwia to uzyskanie lepszych osadzeń dla artykułów,
   nawet mając na uwadze fakt, że adnotacje są dokonane automatycznie i pewien
   odsetek może być niepoprawny.
2. Wszystkie teksty odnośników zostały zastąpione abstrakcyjnymi
   identyfikatorami, wskazującymi jednoznacznie artykuł, do którego prowadzi
   odnośnik. Przykładowo tekst  `Wycieraczka – element wyposażenia samochodu
   albo innego pojazdu wyposażonego w przednią szybę (tramwaj, autobus,
   trolejbus` został transformowany na: `Wycieraczka – element wyposażenia
   ___123___ albo innego pojazdu wyposażonego w przednią szybę (___345___,
   ___789___, ___012___`
3. Na tak spreparowanym tekście uczony był standardowy model Skip-Gram, który
   obok zwykłych słów tworzył osadzenia również dla identyfikatorów artykułów
   w Wikipedii.

> **TODO:** Jak zaznaczyć, że osobiście nie tworzyłem tych embeddingów, ale nie
mam publikacji, która je opisuje i mogę ją zacytować?


## Wyniki algorytmu Entity Linking


|                   | zbiór treningowy | zbiór testowy |
|-------------------|------------------|---------------|
| il. tokenów       | 100 000          | 4300          |
| il. wzmianek      | 100 000          | 6900          |
| accuracy          | 86%              | 86,3%         |
| baseline_accuracy | 91,5%            | 92,2%         |

: Wyniki eksperymentów z algorytmem Entity Linking

Eksperymenty przeprowadzono na próbce danych z racji kosztowności wykonywanych
obliczeń. Badana metoda osiąga dobre wyniki, ale nie udało się uzyskać lepszego
wyniku niż algorytm bazowy.

W procesie analizy wyników odkryto zaskakujący fakt, że zarówno metoda oparta na
kontekście lokalnym oraz metoda oparta na statystykach linków zwraca bardzo
zgodne wyniki. Z przeprowadzonych badań wynika, że tylko w ok 2% przypadków
metoda oparta na lokalnym kontekście zwraca poprawny wynik, kiedy nie zwraca go
metoda bazowa. W związku z tym próby połączenia tych dwóch cech w bardziej
złożony klasyfikator nie dadzą znacząco lepszych rezultatów.

Możliwą hipotezą takiego stanu rzeczy jest, że zarówno metody oparte na
statystykach linków jak i metody oparte na osadzeniach słów/artykułów mają
problemy w przypadkach, gdzie poprawną odpowiedzią jest artykuł, który jest
rzadko linkowany. Modele oparte na statystykach nie mają wystarczających
statystyk na jego temat, a modele wektorowe nie mają dobrze wytrenowanej
reprezentacji wektorowej, bo pojawia się on w tekście stosunkowo rzadko.


## Wizualizacje mechanizmu attention

W tej sekcji przedstawione są wizualizacje wag mechanizmu attention
odpowiedzialnego za wybieranie słów z kontekstu lokalnego, które będą się
składać na reprezentację wektorową kontekstu lokalnego. W przedstawionych
wizualizacjach  na górze znajduje się tekst wzmianki, po lewej znajdują się
słowa z kontekstu lokalnego, na dole znajdują się tytuły artykułów, które są
kandydatami a macierz w centrum reprezentuje wartość wskaźnika korelacji
pomiędzy słowem a kandydatem. Przy słowach z kontekstu, które zostały
wykorzystane do obliczenia reprezentacji wektorowej kontekstu znajdują się wagi
użyte przy średniej ważonej.

![ Wizualizacja dla wzmianki "kadencja" przed skalowaniem
](images/kadencja_nonscaled){#fig:kadencja_nonscaled height=90% width=90%}

Na podstawie @fig:kadencja_nonscaled można zauważyć wysokie wzkaźniki na
przecięciu kandydata _Kandencja(politologia)_ oraz słów powiązanych z polityką,
np _wyborów_, _prezydent_, _senatorów_. Oznacza to, że mechanizm potrafi wyłonić
semantycznie powiązane słowa i artykuły.

![ Wizualizacja dla wzmianki "austriackiego" przed skalowaniem
](images/austriackiego_nonscaled){#fig:austriackiego_nonscaled height=90%
width=90%}

Na podstawie innej wizualizacji @fig:austriackiego_nonscaled,  można zauważyć,
że algorytm wybiera są bardzo powszechne słowa(w tym przypadku _r_ będący
skrótem od _rok_), zamiast skupiać się na słowach bardziej specyficznych, które
mogą lepiej wyznaczyć reprezentację kontekstu. Aby ograniczyć ten niepożądany
efekt zastosowano normalizację osadzeń słów w kontekście. Podzielono je przez
logarytm częstości ich występowania w korpusie, co jest operacją analogiczną do
IDF (ang. Inverse document frequency). Po tej operacji zmniejszyła się liczba
przypadków, gdzie na reprezentację kontekstu składają się powszechne słowa,
które nie niosą wyraźnego znaczenia. Dla przykładu @fig:austriackiego_scaled
przedstawia ten sam przykład po operacji skalowania.


![ Wizualizacja dla wzmianki "austriackiego" po skalowaniu
](images/austriackiego_scaled){#fig:austriackiego_scaled height=90% width=90%}

Jako efekt uboczny skalowania, wartości iloczynu skalarnego są bardziej zbliżone
do zera. Ma to wpływ na wagi przypisywane osadzeniom. Funkcja softmax używa
potęgowania, więc przy normalizacji otrzymamy bardziej _drastyczne_
współczynniki w stylu _0 0 0 1 0 0_. Mniejsze wartości na wejściu pozwalają
uwzględnić więcej komponentów i np. otrzymać rozkład wag _0 0 0.7 0.3 0_ Widać
to przy @fig:kadencja_scaled, gdzie po normalizacji został uwzględniony drugi
powiązany semantycznie wyraz.

![ Wizualizacja dla wzmianki "kadencja" po skalowaniu
](images/kadencja_scaled){#fig:kadencja_scaled height=90% width=90%}

# Podsumowanie

- W tej pracy przedstawiono algorytm Entity Linking oparty na wektorowej
  reprezentacji słów  
- Bazowy algorytm oparty na  statystykach linków osiąga bardzo dobre wyniki,
  których nie udało się przebić za pomocą zaproponowanego algorytmu opartego na
  wektorowych reprezentacjach słów.




[modeline]: # ( vim: set tw=80 fo+=t: )



