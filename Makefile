all: tex  tex_todo bibtex open

tex:
	@pandoc -s -F pandoc-crossref \
						-F ./remove-todo.hs \
						--biblatex \
						--template=praca.tex \
						-f markdown \
						-t latex+raw_tex+tex_math_dollars+citations \
						-o Marcin_Flis_entity_linking.tex \
						main.md
	@pdflatex -halt-on-error \
						-interaction=nonstopmode\
						Marcin_Flis_entity_linking.tex

tex_todo:
	@pandoc -s -F pandoc-crossref \
						--biblatex \
						--template=praca.tex \
						-f markdown \
						-t latex+raw_tex+tex_math_dollars+citations \
						-o Marcin_Flis_entity_linking_with_todo.tex \
						main.md
	@pdflatex -halt-on-error \
						-interaction=nonstopmode\
						Marcin_Flis_entity_linking_with_todo.tex

bibtex:
	@bibtex Marcin_Flis_entity_linking
	@pdflatex Marcin_Flis_entity_linking.tex
	@pdflatex Marcin_Flis_entity_linking.tex
	@bibtex Marcin_Flis_entity_linking_with_todo
	@pdflatex Marcin_Flis_entity_linking_with_todo.tex
	@pdflatex Marcin_Flis_entity_linking_with_todo.tex

clean:
	rm *.pdf *.aux *.log *.bbl *.blg *.out *.toc *run.xml *-blx.bib Marcin_Flis_entity_linking_with_todo.tex Marcin_Flis_entity_linking.tex

open:
	@okular Marcin_Flis_entity_linking.pdf

.PHONY: all clean paper


  

